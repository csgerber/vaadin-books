package com.example.application.repository;

import com.example.application.models.Movie;
import com.example.application.models.VolumesResponse;
import com.example.application.service.AsyncRestCallback;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Repository
public class MovieRepository {


    private static final ExecutorService executorService = Executors.newFixedThreadPool(3);

    @Value("${QUARKUS_BASE_URL:#{'localhost:8080/'}}")
    private String baseUrl;



    //READ paged
    public void getMovies(AsyncRestCallback<List<Movie>> callback, int page) {


        String raw = baseUrl + "movies/paged/" + page;
        WebClient.RequestHeadersSpec<?> spec = WebClient.create().get().uri(raw);

        spec.retrieve().bodyToMono(new ParameterizedTypeReference<List<Movie>>() {}).publishOn(Schedulers.fromExecutor(executorService)).subscribe(result -> {

            final List<Movie> movies = result;
            if (null == movies) return;
            callback.operationFinished(movies);

        });

    }

    //DELETE document
    public void deleteMovie(AsyncRestCallback<Movie> callback, String id){

        String raw = baseUrl + "movies/" + id;
        WebClient.RequestHeadersSpec<?> spec = WebClient.create().delete().uri(raw);

        spec.retrieve().bodyToMono(Movie.class).publishOn(Schedulers.fromExecutor(executorService)).subscribe(result -> {
            final Movie movie = result;
            if (null == movie) return;
            callback.operationFinished(movie);
        });


    }
}
