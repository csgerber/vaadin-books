package com.example.application.repository;

import com.example.application.models.Item;
import com.example.application.models.VolumesResponse;
import com.example.application.service.AsyncRestCallback;
import com.example.application.service.BookService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Repository
public class BookRepository {



    //READ paged
    public void getBooks(AsyncRestCallback<VolumesResponse> callback, String search, int maxResults,
                         int startIndex) {


        String raw = "https://www.googleapis.com/books/v1/volumes?q=%s&maxResults=%d&startIndex=%d";
        String formatted = String.format(raw, search, maxResults, startIndex);
        WebClient.RequestHeadersSpec<?> spec = WebClient.create().get().uri(formatted);

        spec.retrieve().toEntity(VolumesResponse.class).subscribe(result -> {

            final VolumesResponse volumesResponse = result.getBody();

            if (null == volumesResponse.getItems()) return;

            callback.operationFinished(volumesResponse);

        });

    }
}
