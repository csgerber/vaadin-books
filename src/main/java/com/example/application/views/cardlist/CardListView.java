package com.example.application.views.cardlist;

import java.util.ArrayList;
import java.util.List;

import com.example.application.models.Item;
import com.example.application.models.Movie;
import com.example.application.models.VolumesResponse;
import com.example.application.service.MovieService;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.example.application.views.main.MainView;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.component.dependency.CssImport;
import elemental.json.JsonObject;


@Route(value = "card-list", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle("Card List")
@CssImport("./views/cardlist/card-list-view.css")
public class CardListView extends Div implements AfterNavigationObserver {


    private MovieService movieService;
    private Grid<Movie> grid = new Grid<>();
    private int page = 1;
    private boolean isLoading = false;
    private List<Movie> vols;

  //  private List<Movie> items = new ArrayList<>();
   // private TextField textField;
    private String searchTerm;

    public CardListView(MovieService movieService) {
        this.movieService = movieService;
      //  items.clear();
        getMovies();

//        textField = new TextField();
//        textField.setLabel("Search Term");
//        textField.setPlaceholder("search...");
//        textField.setAutofocus(true);
//        textField.setWidthFull();
//        textField.addKeyDownListener(keyDownEvent -> {
//                    String keyStroke = keyDownEvent.getKey().getKeys().toString();
//                    if (keyStroke.equals("[Enter]") && !isLoading) {
//                        System.out.println(textField.getValue());
//                        searchTerm = textField.getValue();
//                        page = 1;
//
//
//                    }
//                }
//        );
        addClassName("card-list-view");
        setSizeFull();
        grid.setHeight("100%");
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
        grid.addComponentColumn(item -> createCard(item));
        grid.addItemClickListener(new ComponentEventListener<ItemClickEvent<Movie>>() {
            @Override
            public void onComponentEvent(ItemClickEvent<Movie> itemItemClickEvent) {
                System.out.println(itemItemClickEvent.getItem());
                movieService.deleteMovie(m -> {
                    getUI().get().access(() -> {
                       // isLoading = false;
                       // UI.getCurrent().getPage().reload();
                        page = 1;
                        getMovies();

                    });
                }, itemItemClickEvent.getItem().getId());
            }
        });

        add( withClientsideScrollListener(grid));
    }

    private void getMovies() {
        isLoading = true;

        movieService.getMovies(volumesResponse -> {
            //we must fire this callback on the UI thread and that is why we use getUi().get().acccess(() ->
            getUI().get().access(() -> {

                //this is the callback result, so volumesResponse is the volumesResponse returned from
                //      void operationFinished(T results);
                //items.addAll(volumesResponse);
                if (page == 1){
                    vols = volumesResponse;
                } else {
                    vols.addAll(volumesResponse);
                }
                grid.setItems(vols);


                page++;
                isLoading = false;
                //https://vaadin.com/docs/v14/flow/advanced/tutorial-push-access
                //we need to notify the browser when we are done. Note that the parent-view MainView is annotated with
                //the @Push annotation, which will force the views to refresh themselves, including the grid.
                getUI().get().push();

            });
        }, page);

    }




    private Grid<Movie> withClientsideScrollListener(Grid<Movie> grid) {
        grid.getElement().executeJs(
                "this.$.scroller.addEventListener('scroll', (scrollEvent) => " +
                        "{requestAnimationFrame(" +
                        "() => $0.$server.onGridScroll({sh: this.$.table.scrollHeight, " +
                        "ch: this.$.table.clientHeight, " +
                        "st: this.$.table.scrollTop}))},true)",
                getElement());
        return grid;
    }

    @ClientCallable
    public void onGridScroll(JsonObject scrollEvent) {
        int scrollHeight = (int) scrollEvent.getNumber("sh");
        int clientHeight = (int) scrollEvent.getNumber("ch");
        int scrollTop = (int) scrollEvent.getNumber("st");

        double percentage = (double) scrollTop / (scrollHeight - clientHeight);
        //reached the absolute bottom of the scroll
        if (percentage == 1.0) {

            if (!isLoading) {
                getMovies();
            }
            grid.scrollToIndex(vols.size() / 2);

        }

    }

    private HorizontalLayout createCard(Movie item) {
        HorizontalLayout card = new HorizontalLayout();
        card.addClassName("card");
        card.setSpacing(false);
        card.getThemeList().add("spacing-s");

        Image image = new Image();
        image.setSrc("https://picsum.photos/seed/picsum/50/50");
        VerticalLayout description = new VerticalLayout();
        description.addClassName("description");
        description.setSpacing(false);
        description.setPadding(false);

        HorizontalLayout header = new HorizontalLayout();
        header.addClassName("header");
        header.setSpacing(false);
        header.getThemeList().add("spacing-s");

        Span name = new Span(item.getTitle());
        name.addClassName("name");
        Span date = new Span(item.getId());
        date.addClassName("date");

        Span year = new Span(String.valueOf(item.getYear()));
        year.addClassName("year");
        header.add(name, date, year);


        description.add(header);
        card.add(image, description);
        return card;
    }

    private String getPublisher(Item item) {
        if (null == item.getVolumeInfo() || null == item.getVolumeInfo().getPublisher()) {
            return "";
        }
        return item.getVolumeInfo().getPublisher();
    }

    private String getAuthor(Item item) {
        if (null == item.getVolumeInfo() || null == item.getVolumeInfo().getAuthors()) {
            return "";
        }
        return item.getVolumeInfo().getAuthors().get(0);
    }

    private String getSmallThumbnail(Item item) {
        if (null == item.getVolumeInfo() || null == item.getVolumeInfo().getImageLinks() || null ==
                item.getVolumeInfo().getImageLinks().getSmallThumbnail()) {
            return "https://randomuser.me/api/portraits/men/16.jpg";
        }
        return item.getVolumeInfo().getImageLinks().getSmallThumbnail();
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        // we don't need to do anything here now. Possibly remove interface and this contract-method.
    }


}
