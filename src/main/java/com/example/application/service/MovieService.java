package com.example.application.service;


import com.example.application.models.Movie;
import com.example.application.models.VolumesResponse;
import com.example.application.repository.BookRepository;
import com.example.application.repository.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MovieService {

    public static final int MAX_RESULTS = 20;
    private MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

   //READ paged
    public void getMovies(AsyncRestCallback<List<Movie>> callback,
                          int page) {

        System.out.println("fetching page -> " + page);

        movieRepository.getMovies(callback, page);


    }

    //DELETE
    public void deleteMovie(AsyncRestCallback<Movie> callback, String id) {

        System.out.println("deleting movie -> " + id);

        movieRepository.deleteMovie(callback, id);


    }

}